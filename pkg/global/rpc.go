package global

const (
	EtcdAddress = "127.0.0.1:2379"
)

// 微服务常量
const (
	ApiGatewayName    = "gateway"
	ApiGatewayAddress = "127.0.0.1:9000"

	ServiceUserName    = "userservice"
	ServiceUserAddress = "127.0.0.1:8082"

	ServiceVideoName    = "videoservice"
	ServiceVideoAddress = ":127.0.0.1:8083"

	ServiceRelationName    = "relationservice"
	ServiceRelationAddress = "127.0.0.1:8084"

	ServiceLikeName    = "likeservice"
	ServiceLikeAddress = "127.0.0.1:8087"

	ServiceCommentName    = "commentservice"
	ServiceCommentAddress = "127.0.0.1:8088"

	ServiceMessageName    = "messageservice"
	ServiceMessageAddress = "127.0.0.1:8100"

	ServiceFeedName    = "feedservice"
	ServiceFeedAddress = "127.0.0.1:8101"
)
