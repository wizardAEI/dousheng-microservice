// Code generated by Kitex v0.4.4. DO NOT EDIT.

package likeservice

import (
	"context"
	like "dousheng/kitex_gen/like"
	"fmt"
	client "github.com/cloudwego/kitex/client"
	kitex "github.com/cloudwego/kitex/pkg/serviceinfo"
	streaming "github.com/cloudwego/kitex/pkg/streaming"
	proto "google.golang.org/protobuf/proto"
)

func serviceInfo() *kitex.ServiceInfo {
	return likeServiceServiceInfo
}

var likeServiceServiceInfo = NewServiceInfo()

func NewServiceInfo() *kitex.ServiceInfo {
	serviceName := "LikeService"
	handlerType := (*like.LikeService)(nil)
	methods := map[string]kitex.MethodInfo{
		"FavoriteAction":  kitex.NewMethodInfo(favoriteActionHandler, newFavoriteActionArgs, newFavoriteActionResult, false),
		"GetFavoriteList": kitex.NewMethodInfo(getFavoriteListHandler, newGetFavoriteListArgs, newGetFavoriteListResult, false),
		"FavoriteCount":   kitex.NewMethodInfo(favoriteCountHandler, newFavoriteCountArgs, newFavoriteCountResult, false),
	}
	extra := map[string]interface{}{
		"PackageName": "like",
	}
	svcInfo := &kitex.ServiceInfo{
		ServiceName:     serviceName,
		HandlerType:     handlerType,
		Methods:         methods,
		PayloadCodec:    kitex.Protobuf,
		KiteXGenVersion: "v0.4.4",
		Extra:           extra,
	}
	return svcInfo
}

func favoriteActionHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	switch s := arg.(type) {
	case *streaming.Args:
		st := s.Stream
		req := new(like.FavoriteActionRequest)
		if err := st.RecvMsg(req); err != nil {
			return err
		}
		resp, err := handler.(like.LikeService).FavoriteAction(ctx, req)
		if err != nil {
			return err
		}
		if err := st.SendMsg(resp); err != nil {
			return err
		}
	case *FavoriteActionArgs:
		success, err := handler.(like.LikeService).FavoriteAction(ctx, s.Req)
		if err != nil {
			return err
		}
		realResult := result.(*FavoriteActionResult)
		realResult.Success = success
	}
	return nil
}
func newFavoriteActionArgs() interface{} {
	return &FavoriteActionArgs{}
}

func newFavoriteActionResult() interface{} {
	return &FavoriteActionResult{}
}

type FavoriteActionArgs struct {
	Req *like.FavoriteActionRequest
}

func (p *FavoriteActionArgs) FastRead(buf []byte, _type int8, number int32) (n int, err error) {
	if !p.IsSetReq() {
		p.Req = new(like.FavoriteActionRequest)
	}
	return p.Req.FastRead(buf, _type, number)
}

func (p *FavoriteActionArgs) FastWrite(buf []byte) (n int) {
	if !p.IsSetReq() {
		return 0
	}
	return p.Req.FastWrite(buf)
}

func (p *FavoriteActionArgs) Size() (n int) {
	if !p.IsSetReq() {
		return 0
	}
	return p.Req.Size()
}

func (p *FavoriteActionArgs) Marshal(out []byte) ([]byte, error) {
	if !p.IsSetReq() {
		return out, fmt.Errorf("No req in FavoriteActionArgs")
	}
	return proto.Marshal(p.Req)
}

func (p *FavoriteActionArgs) Unmarshal(in []byte) error {
	msg := new(like.FavoriteActionRequest)
	if err := proto.Unmarshal(in, msg); err != nil {
		return err
	}
	p.Req = msg
	return nil
}

var FavoriteActionArgs_Req_DEFAULT *like.FavoriteActionRequest

func (p *FavoriteActionArgs) GetReq() *like.FavoriteActionRequest {
	if !p.IsSetReq() {
		return FavoriteActionArgs_Req_DEFAULT
	}
	return p.Req
}

func (p *FavoriteActionArgs) IsSetReq() bool {
	return p.Req != nil
}

type FavoriteActionResult struct {
	Success *like.FavoriteActionResponse
}

var FavoriteActionResult_Success_DEFAULT *like.FavoriteActionResponse

func (p *FavoriteActionResult) FastRead(buf []byte, _type int8, number int32) (n int, err error) {
	if !p.IsSetSuccess() {
		p.Success = new(like.FavoriteActionResponse)
	}
	return p.Success.FastRead(buf, _type, number)
}

func (p *FavoriteActionResult) FastWrite(buf []byte) (n int) {
	if !p.IsSetSuccess() {
		return 0
	}
	return p.Success.FastWrite(buf)
}

func (p *FavoriteActionResult) Size() (n int) {
	if !p.IsSetSuccess() {
		return 0
	}
	return p.Success.Size()
}

func (p *FavoriteActionResult) Marshal(out []byte) ([]byte, error) {
	if !p.IsSetSuccess() {
		return out, fmt.Errorf("No req in FavoriteActionResult")
	}
	return proto.Marshal(p.Success)
}

func (p *FavoriteActionResult) Unmarshal(in []byte) error {
	msg := new(like.FavoriteActionResponse)
	if err := proto.Unmarshal(in, msg); err != nil {
		return err
	}
	p.Success = msg
	return nil
}

func (p *FavoriteActionResult) GetSuccess() *like.FavoriteActionResponse {
	if !p.IsSetSuccess() {
		return FavoriteActionResult_Success_DEFAULT
	}
	return p.Success
}

func (p *FavoriteActionResult) SetSuccess(x interface{}) {
	p.Success = x.(*like.FavoriteActionResponse)
}

func (p *FavoriteActionResult) IsSetSuccess() bool {
	return p.Success != nil
}

func getFavoriteListHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	switch s := arg.(type) {
	case *streaming.Args:
		st := s.Stream
		req := new(like.FavoriteListRequest)
		if err := st.RecvMsg(req); err != nil {
			return err
		}
		resp, err := handler.(like.LikeService).GetFavoriteList(ctx, req)
		if err != nil {
			return err
		}
		if err := st.SendMsg(resp); err != nil {
			return err
		}
	case *GetFavoriteListArgs:
		success, err := handler.(like.LikeService).GetFavoriteList(ctx, s.Req)
		if err != nil {
			return err
		}
		realResult := result.(*GetFavoriteListResult)
		realResult.Success = success
	}
	return nil
}
func newGetFavoriteListArgs() interface{} {
	return &GetFavoriteListArgs{}
}

func newGetFavoriteListResult() interface{} {
	return &GetFavoriteListResult{}
}

type GetFavoriteListArgs struct {
	Req *like.FavoriteListRequest
}

func (p *GetFavoriteListArgs) FastRead(buf []byte, _type int8, number int32) (n int, err error) {
	if !p.IsSetReq() {
		p.Req = new(like.FavoriteListRequest)
	}
	return p.Req.FastRead(buf, _type, number)
}

func (p *GetFavoriteListArgs) FastWrite(buf []byte) (n int) {
	if !p.IsSetReq() {
		return 0
	}
	return p.Req.FastWrite(buf)
}

func (p *GetFavoriteListArgs) Size() (n int) {
	if !p.IsSetReq() {
		return 0
	}
	return p.Req.Size()
}

func (p *GetFavoriteListArgs) Marshal(out []byte) ([]byte, error) {
	if !p.IsSetReq() {
		return out, fmt.Errorf("No req in GetFavoriteListArgs")
	}
	return proto.Marshal(p.Req)
}

func (p *GetFavoriteListArgs) Unmarshal(in []byte) error {
	msg := new(like.FavoriteListRequest)
	if err := proto.Unmarshal(in, msg); err != nil {
		return err
	}
	p.Req = msg
	return nil
}

var GetFavoriteListArgs_Req_DEFAULT *like.FavoriteListRequest

func (p *GetFavoriteListArgs) GetReq() *like.FavoriteListRequest {
	if !p.IsSetReq() {
		return GetFavoriteListArgs_Req_DEFAULT
	}
	return p.Req
}

func (p *GetFavoriteListArgs) IsSetReq() bool {
	return p.Req != nil
}

type GetFavoriteListResult struct {
	Success *like.FavoriteListResponse
}

var GetFavoriteListResult_Success_DEFAULT *like.FavoriteListResponse

func (p *GetFavoriteListResult) FastRead(buf []byte, _type int8, number int32) (n int, err error) {
	if !p.IsSetSuccess() {
		p.Success = new(like.FavoriteListResponse)
	}
	return p.Success.FastRead(buf, _type, number)
}

func (p *GetFavoriteListResult) FastWrite(buf []byte) (n int) {
	if !p.IsSetSuccess() {
		return 0
	}
	return p.Success.FastWrite(buf)
}

func (p *GetFavoriteListResult) Size() (n int) {
	if !p.IsSetSuccess() {
		return 0
	}
	return p.Success.Size()
}

func (p *GetFavoriteListResult) Marshal(out []byte) ([]byte, error) {
	if !p.IsSetSuccess() {
		return out, fmt.Errorf("No req in GetFavoriteListResult")
	}
	return proto.Marshal(p.Success)
}

func (p *GetFavoriteListResult) Unmarshal(in []byte) error {
	msg := new(like.FavoriteListResponse)
	if err := proto.Unmarshal(in, msg); err != nil {
		return err
	}
	p.Success = msg
	return nil
}

func (p *GetFavoriteListResult) GetSuccess() *like.FavoriteListResponse {
	if !p.IsSetSuccess() {
		return GetFavoriteListResult_Success_DEFAULT
	}
	return p.Success
}

func (p *GetFavoriteListResult) SetSuccess(x interface{}) {
	p.Success = x.(*like.FavoriteListResponse)
}

func (p *GetFavoriteListResult) IsSetSuccess() bool {
	return p.Success != nil
}

func favoriteCountHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	switch s := arg.(type) {
	case *streaming.Args:
		st := s.Stream
		req := new(like.FavoriteCountRequest)
		if err := st.RecvMsg(req); err != nil {
			return err
		}
		resp, err := handler.(like.LikeService).FavoriteCount(ctx, req)
		if err != nil {
			return err
		}
		if err := st.SendMsg(resp); err != nil {
			return err
		}
	case *FavoriteCountArgs:
		success, err := handler.(like.LikeService).FavoriteCount(ctx, s.Req)
		if err != nil {
			return err
		}
		realResult := result.(*FavoriteCountResult)
		realResult.Success = success
	}
	return nil
}
func newFavoriteCountArgs() interface{} {
	return &FavoriteCountArgs{}
}

func newFavoriteCountResult() interface{} {
	return &FavoriteCountResult{}
}

type FavoriteCountArgs struct {
	Req *like.FavoriteCountRequest
}

func (p *FavoriteCountArgs) FastRead(buf []byte, _type int8, number int32) (n int, err error) {
	if !p.IsSetReq() {
		p.Req = new(like.FavoriteCountRequest)
	}
	return p.Req.FastRead(buf, _type, number)
}

func (p *FavoriteCountArgs) FastWrite(buf []byte) (n int) {
	if !p.IsSetReq() {
		return 0
	}
	return p.Req.FastWrite(buf)
}

func (p *FavoriteCountArgs) Size() (n int) {
	if !p.IsSetReq() {
		return 0
	}
	return p.Req.Size()
}

func (p *FavoriteCountArgs) Marshal(out []byte) ([]byte, error) {
	if !p.IsSetReq() {
		return out, fmt.Errorf("No req in FavoriteCountArgs")
	}
	return proto.Marshal(p.Req)
}

func (p *FavoriteCountArgs) Unmarshal(in []byte) error {
	msg := new(like.FavoriteCountRequest)
	if err := proto.Unmarshal(in, msg); err != nil {
		return err
	}
	p.Req = msg
	return nil
}

var FavoriteCountArgs_Req_DEFAULT *like.FavoriteCountRequest

func (p *FavoriteCountArgs) GetReq() *like.FavoriteCountRequest {
	if !p.IsSetReq() {
		return FavoriteCountArgs_Req_DEFAULT
	}
	return p.Req
}

func (p *FavoriteCountArgs) IsSetReq() bool {
	return p.Req != nil
}

type FavoriteCountResult struct {
	Success *like.FavoriteCountResponse
}

var FavoriteCountResult_Success_DEFAULT *like.FavoriteCountResponse

func (p *FavoriteCountResult) FastRead(buf []byte, _type int8, number int32) (n int, err error) {
	if !p.IsSetSuccess() {
		p.Success = new(like.FavoriteCountResponse)
	}
	return p.Success.FastRead(buf, _type, number)
}

func (p *FavoriteCountResult) FastWrite(buf []byte) (n int) {
	if !p.IsSetSuccess() {
		return 0
	}
	return p.Success.FastWrite(buf)
}

func (p *FavoriteCountResult) Size() (n int) {
	if !p.IsSetSuccess() {
		return 0
	}
	return p.Success.Size()
}

func (p *FavoriteCountResult) Marshal(out []byte) ([]byte, error) {
	if !p.IsSetSuccess() {
		return out, fmt.Errorf("No req in FavoriteCountResult")
	}
	return proto.Marshal(p.Success)
}

func (p *FavoriteCountResult) Unmarshal(in []byte) error {
	msg := new(like.FavoriteCountResponse)
	if err := proto.Unmarshal(in, msg); err != nil {
		return err
	}
	p.Success = msg
	return nil
}

func (p *FavoriteCountResult) GetSuccess() *like.FavoriteCountResponse {
	if !p.IsSetSuccess() {
		return FavoriteCountResult_Success_DEFAULT
	}
	return p.Success
}

func (p *FavoriteCountResult) SetSuccess(x interface{}) {
	p.Success = x.(*like.FavoriteCountResponse)
}

func (p *FavoriteCountResult) IsSetSuccess() bool {
	return p.Success != nil
}

type kClient struct {
	c client.Client
}

func newServiceClient(c client.Client) *kClient {
	return &kClient{
		c: c,
	}
}

func (p *kClient) FavoriteAction(ctx context.Context, Req *like.FavoriteActionRequest) (r *like.FavoriteActionResponse, err error) {
	var _args FavoriteActionArgs
	_args.Req = Req
	var _result FavoriteActionResult
	if err = p.c.Call(ctx, "FavoriteAction", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetFavoriteList(ctx context.Context, Req *like.FavoriteListRequest) (r *like.FavoriteListResponse, err error) {
	var _args GetFavoriteListArgs
	_args.Req = Req
	var _result GetFavoriteListResult
	if err = p.c.Call(ctx, "GetFavoriteList", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) FavoriteCount(ctx context.Context, Req *like.FavoriteCountRequest) (r *like.FavoriteCountResponse, err error) {
	var _args FavoriteCountArgs
	_args.Req = Req
	var _result FavoriteCountResult
	if err = p.c.Call(ctx, "FavoriteCount", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}
